#include <iostream>

#include "ball.h"
#include "player.h"

class Game
{
private:
    Ball ball;
    Player p1;
    Player p2;

    char **grid;

    void display() const;

public:
    Game();
    ~Game();

    void run();
};