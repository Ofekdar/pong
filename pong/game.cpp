#include <iostream>

#include "ball.cpp"
#include "player.cpp"
#include "constants.cpp"

#include <stdio.h> // for detecting key presses
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include <thread> // for running async
#include <pthread.h>

struct threadInfo
{
    Player &firstPlayer;
    Player &secondPlayer;
    char (*gameGrid)[GAME_WIDTH + 1];
    bool *exit;
};

void *getPlayerMovement(void *arguments);

class Game
{
private:
    Ball ball;
    Player p1;
    Player p2;
    int timesBounced;

    char grid[GAME_HEIGHT + 1][GAME_WIDTH + 1];

    std::clock_t startTime;

    void display() const;

public:
    Game();
    ~Game();

    void run();
};

void Game::display() const
{
    int i = 0, j = 0;
    std::string gridStr = "";

    // appending all characters in grid to string
    // using string.append doesn't work for some reason
    // i can't just print every character on it's own,
    // becuase then the prints comes out very messy
    for (i = 0; i <= GAME_HEIGHT; i++)
    {
        for (j = 0; j <= GAME_WIDTH; j++)
        {
            gridStr += grid[i][j];
        }
    }

    // for some reason, this does some problems when printing
    //std::string gridStr(&grid[0][0], &grid[GAME_HEIGHT][GAME_WIDTH] + 1);
    printf("%s", gridStr.c_str());
    //std::cout << gridStr;
}

Game::Game() : ball(Ball()), p1(Player(0)), p2(Player(GAME_WIDTH - 1)), startTime(std::clock()), timesBounced(0)
{
    // filling all the grid with ' ', the last line with '_' and last char with '0'
    std::fill(&grid[0][0], &grid[0][0] + sizeof(grid) / (GAME_HEIGHT + 1) * GAME_HEIGHT, ' ');
    std::fill(&grid[0][0] + sizeof(grid) / (GAME_HEIGHT + 1) * GAME_HEIGHT, &grid[0][0] + sizeof(grid) - 1, '_');
    grid[GAME_HEIGHT][GAME_WIDTH - 1] = '0';

    // every last char in subarr is '\n'
    for (int i = 0; i <= GAME_HEIGHT; i++)
    {
        grid[i][GAME_WIDTH] = '\n';
    }

    // the players' move function will already draw them on the board
    p1.move(grid, 0);
    p2.move(grid, 0);

    // drawing the first ball on the board
    grid[int(GAME_HEIGHT / HALF)][1] = 'O';
}

Game::~Game() {}

void Game::run()
{
    while (true)
    {
        bool gameOver = false;

        // deploying the key press detector. he must run async because the function is blocking
        struct threadInfo ti = {std::ref(p1), std::ref(p2), grid, &gameOver};
        pthread_t keyPressesThread;
        pthread_create(&keyPressesThread, NULL, &getPlayerMovement, (void *)&ti);

        for (int t = START_TIMER_COUNT; t > 0; t--)
        {
            grid[0][GAME_WIDTH / HALF] = char(t + INT_TO_CHAR);
            display();
            usleep(MICROSECONDS_TO_SECONDS);
        }
        grid[0][GAME_WIDTH / HALF] = ' ';

        while (!gameOver)
        {
            // updating the terminal, current time and the ball position
            display();
            std::clock_t currentTime = std::clock();
            ball.move(grid, currentTime - startTime);
            //std::cout << float(currentTime - startTime) / CLOCKS_PER_SEC << std::endl;
            startTime = currentTime;

            // if the ball is at the edge of one of the players, change it's direction
            if ((ball.getDirection() ? p2 : p1).checkCollision(ball.getX() + (ball.getDirection() ? 1 : -1), ball.getY()))
            {
                ball.changeDirection();
                timesBounced++;
                std::sprintf(grid[GAME_HEIGHT] + GAME_WIDTH - std::to_string(timesBounced).length(), "%d", timesBounced); // updating the score on the board
            }
            gameOver = ball.getX() == (ball.getDirection() ? GAME_WIDTH : 0); // checking if ball has passed one of the players - game over
        }

        std::cout << "\nGame Over!\nPress any key for a new game.\n"
                  << std::endl;
        std::cin.get();

        // waiting for thread to return
        pthread_join(keyPressesThread, NULL);

        // reseting all objects
        ball.reset(grid);
        p1.reset(grid);
        p2.reset(grid);
        timesBounced = 0;
        display();
    }
}

void *getPlayerMovement(void *arguments)
{
    struct threadInfo *ti = (struct threadInfo *)arguments;

    // credit to jsmith (2009) - http://www.cplusplus.com/forum/unices/11910/
    struct termios oldSettings, newSettings;

    tcgetattr(fileno(stdin), &oldSettings);
    newSettings = oldSettings;
    newSettings.c_lflag &= (~ICANON & ~ECHO);
    tcsetattr(fileno(stdin), TCSANOW, &newSettings);

    while (!*(ti->exit))
    {
        fd_set set;
        struct timeval tv;

        tv.tv_sec = 10;
        tv.tv_usec = 0;

        FD_ZERO(&set);
        FD_SET(fileno(stdin), &set);

        int res = select(fileno(stdin) + 1, &set, NULL, NULL, &tv);

        if (res > 0)
        {
            char c;
            read(fileno(stdin), &c, 1);
            switch (c)
            {
            case 'A': // up key
                ti->secondPlayer.move(ti->gameGrid, -1);
                break;

            case 'B': // down key
                ti->secondPlayer.move(ti->gameGrid, 1);
                break;

            case 'w':
                ti->firstPlayer.move(ti->gameGrid, -1);
                break;

            case 's':
                ti->firstPlayer.move(ti->gameGrid, 1);
                break;

            default:
                break;
            }
        }
    }

    tcsetattr(fileno(stdin), TCSANOW, &oldSettings);
    return 0;
}