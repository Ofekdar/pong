#include <ctime>

#include "constants.cpp"

class Ball
{
private:
    float x;
    float y;
    int momentum_x;
    int momentum_y;

public:
    Ball();

    void move(char grid[][GAME_WIDTH + 1], std::clock_t deltaTime);
    void reset(char grid[][GAME_WIDTH + 1]);

    int getX() const;
    int getY() const;

    bool getDirection() const;
    void changeDirection();
};

Ball::Ball() : x(PIXEL_TO_SPACE), y(int(GAME_HEIGHT / HALF * PIXEL_TO_SPACE)), momentum_x(START_MOMENTUM_X), momentum_y(START_MOMENTUM_Y)
{
}

void Ball::move(char grid[][GAME_WIDTH + 1], std::clock_t deltaTime)
{
    // placing a ' ' at the current ball's spot
    grid[int(y / PIXEL_TO_SPACE)][int(x / PIXEL_TO_SPACE)] = ' ';

    x += float(momentum_x * deltaTime) / CLOCKS_PER_SEC;
    y += float(momentum_y * deltaTime) / CLOCKS_PER_SEC;

    // checking that the ball didn't go out of bounds
    if (y <= 0 || y >= GAME_HEIGHT * PIXEL_TO_SPACE)
    {
        y = y <= 0 ? 1 : GAME_HEIGHT * PIXEL_TO_SPACE - 1;
        momentum_y *= -1;
    }

    // placing the ball at his current position (after he moved, or didn't move)
    grid[int(y / PIXEL_TO_SPACE)][int(x / PIXEL_TO_SPACE)] = 'O';
}

void Ball::reset(char grid[][GAME_WIDTH + 1])
{
    // placing a ' ' at the current ball's spot
    grid[int(y / PIXEL_TO_SPACE)][int(x / PIXEL_TO_SPACE)] = ' ';

    // reseting all the fields
    x = PIXEL_TO_SPACE;
    y = int(GAME_HEIGHT / HALF * PIXEL_TO_SPACE);

    momentum_x = START_MOMENTUM_X;
    momentum_y = START_MOMENTUM_Y;

    // placing the ball at his current position
    grid[GAME_HEIGHT / HALF * PIXEL_TO_SPACE][PIXEL_TO_SPACE] = 'O';
}

int Ball::getX() const
{
    return int(x / PIXEL_TO_SPACE);
}

int Ball::getY() const
{
    return int(y / PIXEL_TO_SPACE);
}

bool Ball::getDirection() const
{
    return momentum_x > 0;
}

void Ball::changeDirection()
{
    momentum_x *= -1;
}