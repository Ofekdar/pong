#include "constants.cpp"

class Player
{
private:
    const int x;
    int y;
    const int length;

public:
    Player(const int startX);

    void move(char grid[][GAME_WIDTH + 1], const int amount);
    bool checkCollision(const int pointX, const int pointY) const;
    void reset(char grid[][GAME_WIDTH + 1]);
};

Player::Player(const int startX) : x(startX), length(PLAYER_LEN), y(int(GAME_HEIGHT / HALF - PLAYER_LEN / HALF))
{
}

void Player::move(char grid[][GAME_WIDTH + 1], const int amount)
{
    // cehcking if after moving, the player dosen't go out of bounds
    if (y + amount > 0 && y + amount + PLAYER_LEN <= GAME_HEIGHT)
    {
        // adding the amount to the player's y, then redrawing him on the board
        y += amount;
        for (int i = 0; i < GAME_HEIGHT; i++)
        {
            grid[i][x] = i >= y && i < y + PLAYER_LEN ? (x == 0 ? ']' : '[') : ' ';
        }
    }
}

bool Player::checkCollision(const int pointX, const int pointY) const
{
    return x == pointX && pointY >= y && pointY <= y + length;
}

void Player::reset(char grid[][GAME_WIDTH + 1])
{
    y = int(GAME_HEIGHT / HALF - PLAYER_LEN / HALF);
    move(grid, 0);
}