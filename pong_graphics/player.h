

class Player
{
private:
    const int x;
    int y;
    const int length;

public:
    Player(const int startX, const int colLen);

    void blit(char **grid) const;

    void moveUp();
    void moveDown(const int colLen);

    bool checkCollision(const int pointX, const int pointY) const;
};