#define SCREEN_SIZE_X 1000
#define SCREEN_SIZE_Y 600
#define GAME_FPS 1000 / 60

#define PLAYER_WIDTH SCREEN_SIZE_X / 35
#define PLAYER_HEIGHT SCREEN_SIZE_Y / 4
#define PLAYER_MARGIN SCREEN_SIZE_X / 40
#define PLAYER_MOVE_SPEED 15

#define BALL_SIZE 40
#define BALL_MOMENTUM_X_MAX 10
#define BALL_MOMENTUM_X_MIN 8
#define BALL_MOMENTUM_Y_MAX 12
#define BALL_MOMENTUM_Y_MIN 8

#define HALF 2