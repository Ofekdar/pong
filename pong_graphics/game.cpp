#include <iostream>

#include "ball.cpp"
#include "player.cpp"

class Game
{
private:
    Ball ball;
    Player p1;
    Player p2;

public:
    Game();
    ~Game();

    int run();
};

Game::Game() : ball(Ball()), p1(Player(PLAYER_MARGIN)), p2(Player(SCREEN_SIZE_X - PLAYER_WIDTH - PLAYER_MARGIN))
{
}

Game::~Game() {}

int Game::run()
{
    // ----- Initialize SDL
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        fprintf(stderr, "SDL could not initialize\n");
        return 1;
    }

    // ----- Create window
    SDL_Window *window = SDL_CreateWindow("Pong", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_SIZE_X, SCREEN_SIZE_Y, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (!window)
    {
        fprintf(stderr, "Error creating window.\n");
        return 2;
    }

    // Setup renderer
    SDL_Renderer *renderer = NULL;
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    const Uint8 *state = SDL_GetKeyboardState(NULL);

    bool close = false;
    while (!close)
    {
        // clears the screen
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

        SDL_PumpEvents();
        if (state[SDL_SCANCODE_W])
        {
            p1.move(-PLAYER_MOVE_SPEED);
        }
        if (state[SDL_SCANCODE_S])
        {
            p1.move(PLAYER_MOVE_SPEED);
        }
        if (state[SDL_SCANCODE_UP])
        {
            p2.move(-PLAYER_MOVE_SPEED);
        }
        if (state[SDL_SCANCODE_DOWN])
        {
            p2.move(PLAYER_MOVE_SPEED);
        }

        SDL_Event event;

        // Events management
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                close = true;
            }
        }

        ball.move();
        if (ball.checkCollision(p1.getModel(), p2.getModel()))
        {
            ball.changeDirection();
        }
        else if (ball.checkOutOfBounds())
        {
            p1.reset();
            p2.reset();
            ball.reset();
        }
        /*
        std::string score_text = "1 | 1";
        TTF_Init();
        TTF_Font *Sans = TTF_OpenFont("Sans.ttf", 24);
        SDL_Color textColor = {255, 255, 255, 0};
        SDL_Surface *textSurface = TTF_RenderText_Solid(font, score_text.c_str(), textColor);
        SDL_Texture *text = SDL_CreateTextureFromSurface(renderer, textSurface);
        int text_width = textSurface->w;
        int text_height = textSurface->h;
        SDL_FreeSurface(textSurface);
        SDL_Rect renderQuad = {20, 100 - 30, text_width, text_height};
        SDL_RenderCopy(renderer, text, NULL, &renderQuad);
        SDL_DestroyTexture(text);
        */
        SDL_RenderFillRect(renderer, ball.getModel());
        SDL_RenderFillRect(renderer, p1.getModel());
        SDL_RenderFillRect(renderer, p2.getModel());

        // triggers the double buffers
        // for multiple rendering
        SDL_RenderPresent(renderer);

        // calculates to 60 fps
        SDL_Delay(GAME_FPS);
    }

    // destroy renderer
    SDL_DestroyRenderer(renderer);

    // destroy window
    SDL_DestroyWindow(window);

    // close SDL
    SDL_Quit();

    // Clear winow
    SDL_RenderClear(renderer);

    return 0;
}