#include "constants.cpp"

#include <SDL2/SDL.h>

class Player
{
private:
    SDL_Rect model;

public:
    Player(const int startX);

    void move(const int amount);
    void reset();
    const SDL_Rect *getModel() const;
};

Player::Player(const int startX)
{
    // initializing the player
    model.w = PLAYER_WIDTH;
    model.h = PLAYER_HEIGHT;
    model.x = startX;
    model.y = SCREEN_SIZE_Y / HALF - PLAYER_HEIGHT / HALF;
}

void Player::move(const int amount)
{
    // checking if after moving, the player dosen't go out of bounds
    if (model.y + amount > 0 && model.y + amount + PLAYER_HEIGHT <= SCREEN_SIZE_Y)
    {
        model.y += amount;
    }
}

void Player::reset()
{
    // reseting the player
    model.y = SCREEN_SIZE_Y / HALF - PLAYER_HEIGHT / HALF;
}

const SDL_Rect *Player::getModel() const
{
    return &model;
}