class Ball
{
private:
    int x;
    int y;
    int momentum_x;
    int momentum_y;
    bool goingLeft;

public:
    Ball(const int rowLen, const int colLen);

    void move(const int startY);
    void blit(char **grid) const;

    int getX() const;
    int getY() const;
    //void setX(const int newX);
    //void setY(const int newY);
};