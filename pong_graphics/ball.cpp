#include "constants.cpp"

#include <SDL2/SDL.h>

class Ball
{
private:
    int momentum_x;
    int momentum_y;
    SDL_Rect model;

public:
    Ball();

    void move();

    void reset();
    bool getDirection() const;
    void changeDirection();

    const SDL_Rect *getModel() const;
    bool checkCollision(const SDL_Rect *p1, const SDL_Rect *p2) const;
    bool checkOutOfBounds() const;
};

Ball::Ball()
{
    // initializing the ball
    model.w = BALL_SIZE;
    model.h = BALL_SIZE;
    model.y = SCREEN_SIZE_Y / HALF - BALL_SIZE / HALF;
    model.x = PLAYER_MARGIN + PLAYER_WIDTH;

    momentum_x = BALL_MOMENTUM_X_MIN + std::rand() / ((RAND_MAX + 1u) / BALL_MOMENTUM_X_MAX);
    momentum_y = BALL_MOMENTUM_Y_MIN + std::rand() / ((RAND_MAX + 1u) / BALL_MOMENTUM_Y_MAX);
}

void Ball::move()
{
    // adding the momentum to the ball's coords
    model.x += momentum_x;
    model.y += momentum_y;

    // checking that the ball won't go out of bounds
    if (model.y < 0 || model.y + BALL_SIZE > SCREEN_SIZE_Y)
    {
        model.y = model.y < 0 ? 0 : SCREEN_SIZE_Y - BALL_SIZE;
        momentum_y *= -1;
    }
}

void Ball::reset()
{
    // reseting the ball
    model.y = SCREEN_SIZE_Y / HALF - BALL_SIZE / HALF;
    model.x = PLAYER_MARGIN + PLAYER_WIDTH;
    momentum_x = BALL_MOMENTUM_X_MIN + std::rand() / ((RAND_MAX + 1u) / BALL_MOMENTUM_X_MAX);
    momentum_y = BALL_MOMENTUM_Y_MIN + std::rand() / ((RAND_MAX + 1u) / BALL_MOMENTUM_Y_MAX);
}

bool Ball::getDirection() const
{
    // returning the direction of the ball
    return momentum_x > 0;
}

void Ball::changeDirection()
{
    // changing the direction of the ball
    momentum_x = (BALL_MOMENTUM_X_MIN + std::rand() / ((RAND_MAX + 1u) / BALL_MOMENTUM_X_MAX)) * (momentum_x > 0 ? -1 : 1);
    momentum_y = (BALL_MOMENTUM_Y_MIN + std::rand() / ((RAND_MAX + 1u) / BALL_MOMENTUM_Y_MAX)) * (momentum_y < 0 ? -1 : 1);
}

const SDL_Rect *Ball::getModel() const
{
    return &model;
}

bool Ball::checkCollision(const SDL_Rect *p1, const SDL_Rect *p2) const
{
    // checking if the ball is colliding with any of the players' rects
    return !getDirection() ? (model.x <= (*p1).x + (*p1).w && model.x >= (*p1).x + (*p1).w / HALF && // the ball's x is inside (second half of) p1 and
                              model.y + model.h >= (*p1).y && model.y <= (*p1).y + (*p1).h)
                           :                                                                     // the ball's y is withing p1's y or
               (model.x + model.w >= (*p2).x && model.x + model.w <= (*p2).x + (*p2).w / HALF && // the ball's x is inside (half of) p2 and
                model.y + model.h >= (*p2).y && model.y <= (*p2).y + (*p2).h);                   // the ball's y is withing p2's y
}

bool Ball::checkOutOfBounds() const
{
    // checking if the ball is out of the bounds of the screen
    return model.x + model.w < 0 || model.x > SCREEN_SIZE_X;
}
